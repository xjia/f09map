$(function(){

var precision = 1000;
var map_width = 5000;
var map_height = 2907;
var marker_width = 20;
var marker_height = 20;

var draw_marker = function(marker){
  var left = map_width * marker.x / 1000 - marker_width / 2;
  var top = map_height * marker.y / 1000 - marker_height / 2;
  if (marker.m === undefined || marker.m === "") {
    $('#markers').append('<div class="marker-container">' +
                         '<a class="marker"' +
                         ' href="' + window.sitebase + '/marker.php?x=' + marker.x + '&y=' + marker.y + '"' +
                         ' target="marker-' + marker.x + '-' + marker.y + '"' +
                         ' id="marker-' + marker.x + '-' + marker.y + '"' +
                         ' data-x="' + marker.x + '"' +
                         ' data-y="' + marker.y + '"' +
                         ' title="这里有 ' + marker.k + ' 条记忆"' +
                         ' style="position: relative;' +
                         ' left: ' + left + 'px;' +
                         ' top: ' + top + 'px;"></a></div>');
  } else {
    $('#markers').append('<div class="marker-container">' +
                         '<a class="marker"' +
                         ' href="' + window.sitebase + '/marker.php?x=' + marker.x + '&y=' + marker.y + '"' +
                         ' target="marker-' + marker.x + '-' + marker.y + '"' +
                         ' id="marker-' + marker.x + '-' + marker.y + '"' +
                         ' data-x="' + marker.x + '"' +
                         ' data-y="' + marker.y + '"' +
                         ' title="这里有 ' + marker.k + ' 条记忆<br/>“' + marker.m + ' ...”"' +
                         ' style="position: relative;' +
                         ' left: ' + left + 'px;' +
                         ' top: ' + top + 'px;"></a></div>');
  }
};

var load_markers = function(callback){
  $.blockUI({ message: '<h3>正在加载标记……</h3>' });
  $.get(window.sitebase + '/markers.php', function(markers){
    var i;
    $('#markers').html('');
    for (i = 0; i < markers.length; i++) {
      draw_marker(markers[i]);
    }
    $('.marker').on('contextmenu', function(event){
      var x = $(this).data('x');
      var y = $(this).data('y');
      show_event_modal(x, y);
      event.preventDefault();
      return false;
    }).tooltip({ html: true });
    $.unblockUI();
    if (callback !== undefined) callback();
  }, 'json');
};

var show_event_modal = function(x, y){
  $('.new-event.modal').modal('show');
  $('#event-x').val(x);
  $('#event-y').val(y);
};

var normalize_event = function(e){
  if (!e.offsetX) {
    e.offsetX = (e.pageX - $(e.target).offset().left);
    e.offsetY = (e.pageY - $(e.target).offset().top);
  }
  return e;
}

var $char_count = $('#char-count');

$('#map').on('contextmenu', function(event){
  var e = normalize_event(event);
  var x = Math.round(precision * e.offsetX / map_width);
  var y = Math.round(precision * e.offsetY / map_height);
  show_event_modal(x, y);
  e.preventDefault();
  return false;
});

$('.new-event.modal').on('show', function(){
  $('input', $(this)).val('');
  $('textarea', $(this)).val('');
  $char_count.html('300');
  $('.submit').addClass('disabled');
});

$('.new-event.modal').on('hidden', function(){
  $('.new-event.modal .submit').removeClass('disabled');
  $.unblockUI();
});

$('#content').bind('input propertychange', function(){
  var remlen = 300 - $(this).val().length;
  $char_count.html('' + remlen);
  if (remlen <= 10) {
    $char_count.addClass('superwarn');
  } else {
    $char_count.removeClass('superwarn');
  }
  if (remlen < 0 || remlen >= 300) {
    $('.new-event.modal .submit').addClass('disabled');
  } else {
    $('.new-event.modal .submit').removeClass('disabled');
  }
});

$('.new-event.modal form').submit(function(){
  return false;
});

$('.new-event.modal .submit').click(function(){
  if ($(this).hasClass('disabled')) {
    return false;
  }
  $(this).addClass('disabled');
  $.blockUI({
    message: '<h3>正在保存……</h3>' +
    '<div class="progress progress-striped active">' +
    '<div class="bar">0%</div>' +
    '</div>'
  });
  var bar = $('.progress .bar');
  $('.new-event.modal form').ajaxSubmit({
    beforeSerialize: function($form, options){
      $('#content0').val(base64_encode($('#content').val()));
    },
    beforeSend: function(){
      var percentVal = '0%';
      bar.width(percentVal).html(percentVal);
    },
    uploadProgress: function(event, position, total, percentComplete){
      var percentVal = percentComplete + '%';
      bar.width(percentVal).html(percentVal);
    },
    dataType: 'json',
    success: function(data){
      if (data.status === 'success') {
        $('.new-event.modal').modal('hide');
        load_markers(function(){
          var x = $('#event-x').val();
          var y = $('#event-y').val();
          var id = 'marker-' + x + '-' + y;
          setTimeout(function(){
            $('#' + id).popover({
              html: 'true',
              placement: 'top',
              trigger: 'manual',
              title: '<b>记忆已保存</b>',
              content: '<a href="' + window.sitebase + '/marker.php?x=' + x + '&y=' + y + '#' + data.event_id + '" target="' + id + '">点击这里查看</a>'
            }).popover('show');
            setTimeout(function(){
              $('#' + id).popover('destroy');
            }, 5000);
          }, 500);
        });
      } else {
        $.unblockUI();
        setTimeout(function(){
          window.alert(data.reason);
          $('.new-event.modal .submit').removeClass('disabled');
        }, 500);
      }
    },
    error: function(){
      window.alert('未知错误');
      window.location.reload();
    }
  });
});

$('body').scrollTop(1310).scrollLeft(1750).dragscrollable({
  dragSelector: '.drag:first',
  acceptPropagatedEvent: false
});

window.scroll_to_id = function(id){
  var window_width = $(window).width();
  var window_height = $(window).height();
  var position = $(id).position();
  var sTop = position.top - window_height/2;
  var sLeft = position.left - window_width/2;
  $('#viewer').scrollTop(sTop).scrollLeft(sLeft);
  return $(id);
};

load_markers(function(){
  window.setTimeout(function(){ $('#hint').fadeOut(); }, 10000);
  $('#splash').hide();
  if (window.load_marker_callback !== undefined) {
    window.load_marker_callback();
  }
  var oldTop = $('body').scrollTop();
  var oldLeft = $('body').scrollLeft();
  $(document).scroll(function(){
    $(document).scrollTop(oldTop).scrollLeft(oldLeft);
    $(document).unbind('scroll');
  });
  $('.edge').mouseover(function(){
    var $viewer = $('body');
    var x = parseInt($(this).data('x'));
    var y = parseInt($(this).data('y'));
    var t = 300;
    var f = function(){
      var sLeft = $viewer.scrollLeft() + x;
      var sTop = $viewer.scrollTop() + y;
      $viewer.animate({ scrollLeft: sLeft, scrollTop: sTop }, t, 'linear');
    };
    var id = setInterval(f, t);
    $(this).mouseout(function(){ clearInterval(id); });
  });
});

});
