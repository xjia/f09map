<?php require(__DIR__ . '/inc/base.php'); ?><!DOCTYPE html>
<html lang="zh-CN">
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="apple-mobile-web-app-capable" content="yes"/>
  <title>闵行交大记忆地图</title>
  <link rel="shortcut icon" href="http://static.xiao-jia.com/f09/favicon.png"/>
  <link rel="apple-touch-icon" href="http://static.xiao-jia.com/f09/favicon.png"/>
  <link rel="stylesheet" href="http://static.xiao-jia.com/f09/tb.css"/>
  <link rel="stylesheet" href="http://static.xiao-jia.com/f09/override.css"/>
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body>
<div id="splash" style="background:white;width:100%;height:5000px;z-index:99999;position:absolute;top:0;left:0;">页面载入中…… 如果长时间没有响应，请刷新页面重试 :-)</div>
<div id="hint"><img src="http://static.xiao-jia.com/f09/lmouse.png"/>左键点击脚印查看记忆，<img src="http://static.xiao-jia.com/f09/rmouse.png"/>右键点击地图标记新的记忆</div>
<div id="viewer">
  <div id="markers"></div>
  <img id="map" class="drag" src="http://static.xiao-jia.com/f09/map.jpg"/>
</div>
<?php require(__DIR__ . '/inc/_form.php'); ?>
<a style="z-index:999;position:fixed;top:2px;left:2px;" href="<?php echo BASE; ?>/recent.php" target="recent" class="recent btn btn-primary">最新记忆</a>
<a style="z-index:999;position:fixed;top:2px;left:87px;" href="<?php echo BASE; ?>/recentpic.php" target="recentpic" class="recent btn btn-warning">最新照片</a>
<div class="edge" style="z-index:990;position:fixed;top:0;left:32px;right:32px;height:32px;cursor:n-resize;" data-x="0" data-y="-100"></div>
<div class="edge" style="z-index:990;position:fixed;bottom:0;left:32px;right:32px;height:32px;cursor:s-resize;" data-x="0" data-y="100"></div>
<div class="edge" style="z-index:990;position:fixed;top:32px;left:0;bottom:32px;width:32px;cursor:w-resize;" data-x="-100" data-y="0"></div>
<div class="edge" style="z-index:990;position:fixed;top:32px;right:0;bottom:32px;width:32px;cursor:e-resize;" data-x="100" data-y="0"></div>
<div class="edge" style="z-index:990;position:fixed;left:0;bottom:0;width:32px;height:32px;cursor:sw-resize;" data-x="-100" data-y="100"></div>
<div class="edge" style="z-index:990;position:fixed;right:0;bottom:0;width:32px;height:32px;cursor:se-resize;" data-x="100" data-y="100"></div>
<div class="edge" style="z-index:990;position:fixed;top:0;right:0;width:32px;height:32px;cursor:ne-resize;" data-x="100" data-y="-100"></div>
<script src="http://static.xiao-jia.com/f09/all.js"></script>
<script>window.sitebase='<?php echo BASE; ?>';</script>
<?php if (isset($_GET['id'])): ?>
<script>window.load_marker_callback=function(){window.scroll_to_id('#<?php echo $_GET['id']; ?>').tooltip('show');};</script>
<?php endif; ?>
<script src="http://static.xiao-jia.com/f09/app.js"></script>
<?php require(__DIR__ .'/inc/_ga.php'); ?>
</body>
</html>
