<?php
require(__DIR__ . '/inc/main.php');

echo '<pre>';

$pwd = fRequest::get('pwd');
if (md5($pwd) !== '29124f93c476088d168aea96790de396') {
  echo 'wrong password';
  exit();
}

global $cache;
$ecount = 0;
$events = fRecordSet::build('Event', array(), array('create_time' => 'asc'));
foreach ($events as $event) {
  $x = $event->getX();
  $y = $event->getY();
  $cache->set("latest:$x:$y", fHTML::encode(
    mb_substr(preg_replace('/\s+/', ' ', $event->getContent()), 0, 30)
  ));
  $ecount++;
}
echo $ecount;
