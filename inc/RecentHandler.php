<?php
class RecentHandler extends CachingHandler
{
  protected function getContentType()
  {
    return 'text/html';
  }
  
  protected function getCacheKey()
  {
    return "recent";
  }

  protected function render()
  {
    $events = fRecordSet::build('Event', array(), array('create_time' => 'desc'), 30);
    $page_title = '最新记忆';
    require(__DIR__ . '/EventListView.php');
  }
}
