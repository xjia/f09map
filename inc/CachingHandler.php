<?php
abstract class CachingHandler
{
  abstract protected function getContentType();
  abstract protected function getCacheKey();
  abstract protected function render();

  public function run()
  {
    global $cache;
    $cache_key = $this->getCacheKey();
    $cached_value = $cache->get($cache_key);
    if ($cached_value === NULL) {
      fBuffer::startCapture();
      $this->render();
      $cached_value = fBuffer::stopCapture();
      $cache->set($cache_key, $cached_value);
    }
    fBuffer::start(TRUE);
    header('Content-Type: ' . $this->getContentType());
    echo $cached_value;
    fBuffer::stop();
  }
}
