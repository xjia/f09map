<?php
define('DB_NAME', 'f09map');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_HOST', 'localhost');

$memcache = new Memcache();
$memcache->connect('localhost', 11211);

/**
 * Can use Flourish classes here :-)
 */
$cache = new fCache('memcache', $memcache);

define('ENABLE_SCHEMA_CACHING', TRUE);
