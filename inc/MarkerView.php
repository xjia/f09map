<?php require(__DIR__ . '/base.php'); ?><!DOCTYPE html>
<html lang="zh-CN">
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>此间的记忆 &middot; 闵行交大记忆地图</title>
  <link rel="shortcut icon" href="http://static.xiao-jia.com/f09/favicon.png"/>
  <link rel="apple-touch-icon" href="http://static.xiao-jia.com/f09/favicon.png"/>
  <link rel="stylesheet" href="http://static.xiao-jia.com/f09/tb.css"/>
  <link rel="stylesheet" href="http://static.xiao-jia.com/f09/events.css"/>
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body>
<div id="wrap">
  <div class="container">
    <div class="page-header">
      <a id="add-event" data-x="<?php echo $x; ?>" data-y="<?php echo $y; ?>" href="javascript:void(0);" class="btn btn-success pull-right">添加我的记忆</a>
      <h1>此间的记忆</h1>
    </div>
    <div>
      <div class="jiathis_style">
        <span class="jiathis_txt">分享到：</span>
        <a class="jiathis_button_qzone">QQ空间</a>
        <a class="jiathis_button_tsina">新浪微博</a>
        <a class="jiathis_button_tqq">腾讯微博</a>
        <a class="jiathis_button_renren">人人网</a>
        <a class="jiathis_button_copy">复制网址</a>
        <a href="http://www.jiathis.com/share?uid=1529617" class="jiathis jiathis_txt jiathis_separator jtico jtico_jiathis" target="_blank">更多</a>
        <a class="jiathis_counter_style"></a>
      </div>
      <div class="clearfix"></div>
    </div>
    <?php foreach ($events as $event): ?>
    <?php include(__DIR__ . "/_event.php"); ?>
    <?php endforeach; ?>
  </div>
  <div id="push"></div>
</div>
<div id="footer">
  <div class="container">
    <p class="muted credit">This website is <a href="https://github.com/stfairy/f09map">open source software</a> developed by <a href="http://xiao-jia.com/">Xiao Jia</a>. See more <a href="<?php echo BASE; ?>/credits.php" target="credits">credits</a>.</p>
  </div>
</div>
<?php require(__DIR__ . '/_form.php'); ?>
<script src="http://static.xiao-jia.com/f09/all.js"></script>
<script src="http://static.xiao-jia.com/f09/events.js"></script>
<script>var jiathis_config={data_track_clickback:'true'};</script>
<script src="http://v3.jiathis.com/code/jia.js?uid=1529617" async="async"></script>
<?php require(__DIR__ .'/_ga.php'); ?>
</body>
</html>
