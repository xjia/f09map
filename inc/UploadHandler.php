<?php
class UploadHandler
{
  public function run()
  {
    if (!fRequest::isPost()) return;
    header('Content-Type: application/json');
    try {
      global $cache;
      
      $visit_key = 'visit:' . $_SERVER['REMOTE_ADDR'];
      if ($cache->get($visit_key, FALSE)) throw new fValidationException('慢点发，捉毛急');
      $cache->set($visit_key, TRUE, 10); # 10 seconds

      $x = fRequest::get('x', 'integer');
      $y = fRequest::get('y', 'integer');
      $content = trim(fRequest::get('content0'));
      $content = trim(base64_decode($content));
      
      $content_key = 'content:' . $_SERVER['REMOTE_ADDR'] . ':' . md5($content);
      if ($cache->get($content_key, FALSE)) throw new fValidationException('慢点发，捉毛急');
      $cache->set($content_key, TRUE, 60 * 5);  # 5 minutes
      
      if ($x <= 0 or $x >= 1000) throw new fValidationException('X should be between 0 and 1000');
      if ($y <= 0 or $y >= 1000) throw new fValidationException('Y should be between 0 and 1000');
      if (mb_strlen($content) < 2) throw new fValidationException('记忆描述太短');
      if (mb_strlen($content) > 300) throw new fValidationException('记忆描述太长');
      
      // handle photo uploading here
      if ($_FILES['photo1']['size'] >= 3 * 1024 * 1024) {
        throw new fValidationException('照片太大，请缩小到3MB以内再上传');
      }
      if (array_key_exists('photo1', $_FILES) and $_FILES['photo1']['size'] > 0) {
        $uploaddir = __DIR__ . '/../photo/';
        $photo_name = sha1(time() . $content);
        $uploadfile = $uploaddir . $photo_name;
        if (move_uploaded_file($_FILES['photo1']['tmp_name'], $uploadfile) === FALSE) {
          throw new fValidationException('保存照片失败');
        }
        if (getimagesize($uploadfile) === FALSE) {
          throw new fValidationException('无法识别的照片格式');
        }
        $photos = array($photo_name);
      } else {
        $photos = array();
      }
      
      $event = new Event();
      $event->setX($x);
      $event->setY($y);
      $event->setContent($content);
      $event->setPhotos(json_encode($photos));
      $event->setIpAddress($_SERVER['REMOTE_ADDR']);
      $event->store();

      global $cache;
      $cache->delete('markers');
      $cache->delete("marker:$x:$y");
      $cache->delete('recent');
      if (count($photos) > 0) {
        $cache->delete('recentpic');
      }
      $cache->set("latest:$x:$y", fHTML::encode(
        mb_substr(preg_replace('/\s+/', ' ', $event->getContent()), 0, 30)
      ));

      $lastmod = time();
      $cache->set('lastmod:markers', $lastmod);
      $cache->set("lastmod:marker:$x:$y", $lastmod);
      $cache->set('lastmod:recent', $lastmod);
      if (count($photos) > 0) {
        $cache->set('lastmod:recentpic', $lastmod);
      }
      
      echo json_encode(array(
        'status' => 'success',
        'event_id' => $event->getId()
      ));
    }
    catch (fException $e) {
      echo json_encode(array(
        'status' => 'failure',
        'reason' => $e->getMessage()
      ));
    }
  }
}
