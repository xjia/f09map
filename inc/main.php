<?php
error_reporting(E_ALL & ~E_NOTICE);
date_default_timezone_set('Asia/Shanghai');
set_time_limit(600);  // 10 minutes
mb_internal_encoding('UTF-8');

function __autoload($class_name)
{
  $flourish_root = __DIR__ . '/flourish/';
  $file = $flourish_root . $class_name . '.php';
  if (file_exists($file)) {
    include($file);
    return;
  }
  throw new Exception('The class ' . $class_name . ' could not be loaded');
}

require(__DIR__ . '/config.php');
require(__DIR__ . '/lastmod.php');

fORMDatabase::attach(new fDatabase('mysql', DB_NAME, DB_USER, DB_PASS, DB_HOST));
if (ENABLE_SCHEMA_CACHING) {
  fORM::enableSchemaCaching($cache);
}

function shorten_ip($ip)
{
  if (strlen($ip) > 15) return substr($ip, strlen($ip)-19, 19);
  return $ip;
}

require(__DIR__ . '/Event.php');
require(__DIR__ . '/CachingHandler.php');
require(__DIR__ . '/MarkersHandler.php');
require(__DIR__ . '/MarkerHandler.php');
require(__DIR__ . '/UploadHandler.php');
require(__DIR__ . '/RecentHandler.php');
require(__DIR__ . '/RecentPicHandler.php');
