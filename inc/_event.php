<?php
$sitebase = fURL::getDomain().BASE;
$x = $event->getX();
$y = $event->getY();
$permalink = "$sitebase/marker.php?x=$x&y=$y#".$event->getId();
$photos = json_decode($event->getPhotos());
$first_photo = count($photos) > 0 ? "$sitebase/photo/".$photos[0] : '';
$content_text = fHTML::encode(preg_replace('/\s+/', ' ', $event->getContent()));
?>
<div id="<?php echo $event->getId(); ?>" class="media well clearfix">
  <blockquote>
    <p class="lead wrap"><?php echo fHTML::encode($event->getContent()); ?></p>
    <small>
      来自 <?php echo fHTML::encode(shorten_ip($event->getIpAddress())); ?> 的同学在 <?php echo $event->getCreateTime(); ?> 添加了这条记忆
      &middot;
      <a href="<?php echo BASE; ?>/?id=<?php echo "marker-$x-$y"; ?>" target="home">查看位置</a>
      &middot;
      <a class="permalink" href="<?php echo $permalink; ?>" rel="tooltip" title="永久链接">#</a>
<script type="text/javascript">
(function(){
var x={
url:'<?php echo $permalink; ?>',
type:'3',count:'1',appkey:'',
title:'<?php echo $content_text; ?>',
pic:'<?php echo $first_photo; ?>',
ralateUid:'',language:'zh_cn',rnd:new Date().valueOf()},t=[];for(var p in x)t.push(p+'='+encodeURIComponent(x[p]));
document.write('<iframe class="pull-right" allowTransparency="true" frameborder="0" scrolling="no" src="http://hits.sinajs.cn/A1/weiboshare.html?'+t.join('&')+'" width="40" height="16"></iframe>');
})()
</script>
    </small>
  </blockquote>
  <?php foreach (json_decode($event->getPhotos()) as $photo): ?>
    <div class="media"><img src="<?php echo BASE; ?>/photo/<?php echo $photo; ?>" class="img-polaroid"/></div>
  <?php endforeach; ?>
</div>
