<div class="new-event modal hide fade">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 class="modal-title">标记新的记忆</h3>
  </div>
  <div class="modal-body">
    <form class="form-horizontal" action="<?php echo BASE; ?>/upload.php" method="POST" enctype="multipart/form-data">
      <input type="hidden" id="event-x" name="x"/>
      <input type="hidden" id="event-y" name="y"/>
      <div class="part">
        <textarea id="content" name="content" placeholder="在此输入记忆描述"></textarea>
        <input type="hidden" id="content0" name="content0"/>
      </div>
      <div class="part">
        <a href="<?php echo BASE; ?>/emotions.php" target="emotions" class="btn pull-left">表情</a>
        <div class="fileupload fileupload-new pull-left" data-provides="fileupload">
          <span class="btn btn-file"><span class="fileupload-new">上传照片</span><span class="fileupload-exists">更换照片</span><input type="file" name="photo1"/></span>
          <span class="fileupload-preview"></span>
          <a href="javascript:void(0);" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a>
        </div>
        <a href="javascript:void(0);" class="submit btn btn-success pull-right">保存</a>
        <div class="muted pull-right char-counter"><span id="char-count">300</span></div>
        <div class="clearfix"></div>
      </div>
    </form>
  </div>
</div>