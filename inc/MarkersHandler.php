<?php
class MarkersHandler extends CachingHandler
{
  protected function getContentType()
  {
    return 'application/json';
  }
  
  protected function getCacheKey()
  {
    return 'markers';
  }

  protected function render()
  {
    $db = fORMDatabase::retrieve();
    $result = $db->translatedQuery('SELECT x,y,count(1) as k FROM events GROUP BY x,y');
    
    $data = array();
    foreach ($result as $row) {
      $data[] = array(
        'x' => $row['x'],
        'y' => $row['y'],
        'k' => $row['k'],
        'm' => $this->getLatestEventContent($row['x'], $row['y'])
      );
    }
    
    echo json_encode($data);
  }

  private function getLatestEventContent($x, $y)
  {
    global $cache;
    return $cache->get("latest:$x:$y", "");
  }
}
