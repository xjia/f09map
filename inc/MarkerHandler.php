<?php
class MarkerHandler extends CachingHandler
{
  protected function getContentType()
  {
    return 'text/html';
  }
  
  protected function getCacheKey()
  {
    $x = fRequest::get('x', 'integer');
    $y = fRequest::get('y', 'integer');
    return "marker:$x:$y";
  }

  protected function render()
  {
    $x = fRequest::get('x', 'integer');
    $y = fRequest::get('y', 'integer');
    $events = fRecordSet::build('Event', array('x=' => $x, 'y=' => $y), array('create_time' => 'desc'));
    require(__DIR__ . '/MarkerView.php');
  }
}
