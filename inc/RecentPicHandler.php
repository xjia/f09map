<?php
class RecentPicHandler extends CachingHandler
{
  protected function getContentType()
  {
    return 'text/html';
  }
  
  protected function getCacheKey()
  {
    return "recentpic";
  }

  protected function render()
  {
    $events = fRecordSet::build('Event', array('photos!' => '[]'), array('create_time' => 'desc'), 10);
    $page_title = '最新照片';
    require(__DIR__ . '/EventListView.php');
  }
}
