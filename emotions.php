<?php require(__DIR__ . '/inc/base.php'); ?><!DOCTYPE html>
<html lang="zh-CN">
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>表情 &middot; 闵行交大记忆地图</title>
  <link rel="shortcut icon" href="http://static.xiao-jia.com/f09/favicon.png"/>
  <link rel="apple-touch-icon" href="http://static.xiao-jia.com/f09/favicon.png"/>
  <link rel="stylesheet" href="http://static.xiao-jia.com/f09/tb.css"/>
  <link rel="stylesheet" href="http://static.xiao-jia.com/f09/events.css"/>
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body>
<div id="wrap">
  <div class="container">
    <div class="page-header">
      <a href="<?php echo BASE; ?>/" target="home" class="btn btn-success pull-right">返回主页</a>
      <h1>表情</h1>
    </div>
    <p class="lead">鼠标移到表情上面可以看到对应的使用方法</p>
    <p class="lead">比如第一个表情“不”，就是输入“(bu)”</p>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/bu.gif" title="(bu)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/chanxiao.gif" title="(chanxiao)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/chifan.gif" title="(chifan)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/cry.gif" title="(cry)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/daxiao.gif" title="(daxiao)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/ganga.gif" title="(ganga)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/haixiu.gif" title="(haixiu)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/han.gif" title="(han)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/jingkong.gif" title="(jingkong)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/jingya.gif" title="(jingya)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/jiong.gif" title="(jiong)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/keai.gif" title="(keai)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/kouzhao.gif" title="(kouzhao)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/ku.gif" title="(ku)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/kun.gif" title="(kun)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/liukoushui.gif" title="(liukoushui)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/mao.gif" title="(mao)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/nanguo.gif" title="(nanguo)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/outu.gif" title="(outu)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/se.gif" title="(se)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/shengbing.gif" title="(shengbing)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/shengqi.gif" title="(shengqi)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/shudaizi.gif" title="(shudaizi)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/tanqi.gif" title="(tanqi)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/taoqi.gif" title="(taoqi)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/tian.gif" title="(tian)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/tiaopi.gif" title="(tiaopi)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/touxiao.gif" title="(touxiao)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/weixiao.gif" title="(weixiao)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/wen.gif" title="(wen)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/yun.gif" title="(yun)"/>
    <img class="emotion" src="http://acm.sjtu.edu.cn/xiaoyou/images/emotions/zhuzui.gif" title="(zhuzui)"/>
  </div>
  <div id="push"></div>
</div>
<div id="footer">
  <div class="container">
    <p class="muted credit">This website is <a href="https://github.com/stfairy/f09map">open source software</a> developed by <a href="http://xiao-jia.com/">Xiao Jia</a>. See more <a href="<?php echo BASE; ?>/credits.php" target="credits">credits</a>.</p>
  </div>
</div>
<script src="http://static.xiao-jia.com/f09/all.js"></script>
<script>$('.emotion').tooltip({ placement: 'top' });</script>
<?php require(__DIR__ .'/inc/_ga.php'); ?>
</body>
</html>
